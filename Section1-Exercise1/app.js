new Vue({
    el: "#exercise",
    data: {
        //Exercise 1
        yourName: "Paulo Soares",
        myAge: 34,
        //Exercise 2
        age:34,     //age=myAge did not work, why? 
        multiplier: 1,
        //Exercise 3
        rNum: 0,
        //Exercise 4
        imgLink: "http://www.oneyoufeed.net/wp-content/uploads/2016/03/persistence.jpg",
    },
    methods: {
        //Q2 - Version1
        // ageMultiplier: function(event) {
        //     this.myAge= this.myAge*3;
        // }
        //Q2 - Version2
        ageMultiplier: function(event) {
            this.multiplier=event.target.value;
            this.age = this.myAge*event.target.value;
        },
        randomNumber: function() {
            this.rNum = Math.random();
        }
    }
});